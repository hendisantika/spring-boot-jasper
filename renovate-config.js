module.exports = {
    platform: 'gitlab',
    endpoint: process.env.CI_API_V4_URL,
    token: process.env.BOT_TOKEN,
    gitAuthor: 'Renovate Bot <hendisantika@yahoo.co.id>',
    labels: ['renovate', 'dependencies', 'automated'],
    assignees: ['hendisantika'],
    onboarding: true,
    onboardingConfig: {
        extends: ['config:recommended'],
    },
    repositories: [process.env.CI_PROJECT_PATH]
};
