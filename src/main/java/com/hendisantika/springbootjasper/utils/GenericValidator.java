package com.hendisantika.springbootjasper.utils;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/02/18
 * Time: 17.01
 * To change this template use File | Settings | File Templates.
 */
public class GenericValidator implements Serializable {
    private static final long serialVersionUID = -5751397309060633864L;

    public static boolean isBlankOrNull(String value) {
        return (value == null) || (value.trim().length() == 0);
    }
}
