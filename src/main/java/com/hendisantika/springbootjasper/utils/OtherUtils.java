package com.hendisantika.springbootjasper.utils;

import java.io.UnsupportedEncodingException;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/02/18
 * Time: 17.02
 * To change this template use File | Settings | File Templates.
 */
public class OtherUtils {
    public static final String BLANK = "";

    public static String reValueNull(String txtValue) {

        if (txtValue == null) {
            txtValue = BLANK;
        }
        if ("null".equals(txtValue)) {
            txtValue = BLANK;
        }

        return txtValue;
    }

    public static Object encodeStringToUTF8(final String value) {
        if (value == null || "".equals(value))
            return "";
        try {
            return new String(value.getBytes("ISO-8859-1"), "UTF-8");
        } catch (final UnsupportedEncodingException e) {
            throw new RuntimeException("Encoding not supported", e);
        }
    }
}
