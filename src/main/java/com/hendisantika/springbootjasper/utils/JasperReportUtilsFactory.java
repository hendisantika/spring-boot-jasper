package com.hendisantika.springbootjasper.utils;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/02/18
 * Time: 17.00
 * To change this template use File | Settings | File Templates.
 */
public class JasperReportUtilsFactory {
    public static JasperReport getReportByName(File reportFile) throws Exception {
        return (JasperReport) JRLoader.loadObject(reportFile);
    }

    public static JRPrintServiceExporter getJRPrintServiceExporter() {
        final JRPrintServiceExporter jasperExporter = new JRPrintServiceExporter();
        return jasperExporter;
    }

    public static void executeExportToPrinter(JasperPrint jasperPrint) throws Exception {
        PrintService service = PrintServiceLookup.lookupDefaultPrintService();
        JRPrintServiceExporter exporter = getJRPrintServiceExporter();
        exporter.setParameter(JRPrintServiceExporterParameter.JASPER_PRINT, jasperPrint);
        exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET, service.getAttributes());
        exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
        exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
        exporter.exportReport();
    }

    public static void executeExportToPrinter(List<?> list, Map<String, Object> parameters, File reportFile) throws Exception {
        JasperReport report = null;
        if (reportFile != null) {
            report = getReportByName(reportFile);
            executeExportToPrinter(JasperFillManager.fillReport(report, parameters, new JRBeanCollectionDataSource(list)));
        }
    }
}
