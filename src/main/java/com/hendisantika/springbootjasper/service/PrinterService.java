package com.hendisantika.springbootjasper.service;

import com.hendisantika.springbootjasper.entity.Slip;
import com.hendisantika.springbootjasper.utils.JasperReportUtilsFactory;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/02/18
 * Time: 16.59
 * To change this template use File | Settings | File Templates.
 */
public class PrinterService {
    public void execute(List<Slip> slips, File reportFile) throws Exception {
        Map<String, Object> parameters = null;
        try {
            parameters = new HashMap<String, Object>();
            parameters.put("p_title", slips.get(0).getTitle());
            JasperReportUtilsFactory.executeExportToPrinter(slips, parameters, reportFile);
        } catch (Exception exception) {

            throw exception;
        }
    }
}
