package com.hendisantika.springbootjasper.controller;

import com.hendisantika.springbootjasper.entity.JsonResponse;
import com.hendisantika.springbootjasper.entity.Slip;
import com.hendisantika.springbootjasper.service.PrinterService;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/02/18
 * Time: 17.13
 * To change this template use File | Settings | File Templates.
 */

@RestController
public class PrintSlipController {
//	private final static Logger logger = Logger.getLogger(PrintSlipController.class);

    @PostMapping(value = "/printSlip")
    public ResponseEntity<?> printSlip(@RequestBody List<Slip> slips) {
        try {
            PrinterService service = new PrinterService();
            Resource resource = new ClassPathResource("jrxml/slip.jasper");

            service.execute(slips, resource.getFile());

            JsonResponse jsonResp = new JsonResponse();
            jsonResp.setDesc("Slip is being printed out on the printer...");
            return new ResponseEntity<>(jsonResp, HttpStatus.OK);
        } catch (Exception ex) {
            String errorMessage;
            errorMessage = ex + " <== error";
            return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
    }
}
