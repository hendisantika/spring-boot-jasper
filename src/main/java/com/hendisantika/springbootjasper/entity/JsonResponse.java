package com.hendisantika.springbootjasper.entity;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-jasper
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 25/02/18
 * Time: 16.58
 * To change this template use File | Settings | File Templates.
 */
public class JsonResponse {
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
