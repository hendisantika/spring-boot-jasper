module.exports = {
    onboardingConfig: { // This will be placed into repositories without config
        extends: ["config:base"],
    },
    platform: "gitlab",
    endpoint: process.env.CI_API_V4_URL, // Needed only for self-hosted Gitlab
    logFile: "renovate.log.json", // Path to log file, must be same as in .gitlab-ci.yml in artifacts
    // Author cannot be the same as any developer, the best is to have own account for Renovate bot
    // Optionally it is possible to change just email
    // See here: https://github.com/renovatebot/config-help/issues/839
    gitAuthor: "RenovateBot <hendisantika+renovate@yopmail.com>",
    labels: ['renovate', 'dependencies', 'automated'],
    baseBranches: ["master"],
    assignees: ["hendisantika"], // New MR are assigned into this person(s)
    // All repositories where Renovate will operate
    onboarding: true,
    onboardingConfig: {
        extends: ['config:recommended'],
    },
    repositories: [
        "hendisantika/renovate-bot", // Updating itself, ie version in .gitlab-ci.yml
        "hendisantika/docker-ftp-deployer",
        process.env.CI_PROJECT_PATH
    ],
};
