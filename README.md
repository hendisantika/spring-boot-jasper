# Spring Boot Jasper Report

### Things todo list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-jasper.git`
2. Navigate to the folder: `cd spring-boot-jasper`
3. Run the application: `mvn clean spring-boot:run`
4. Run this command:

```shell
POST http://localhost:8080/printSlip
Content-Type: application/json

[
  {
    "title": "title 1",
    "item": "item 1"
  },
  {
    "title": "title 2",
    "item": "item 2"
  },
  {
    "title": "title 3",
    "item": "item 3"
  }
]
```
